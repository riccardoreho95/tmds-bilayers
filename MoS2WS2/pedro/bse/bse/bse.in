#                                                                     
# :   :::   :::     ::::    ::::  :::::::::   ::::::::                
# :+:   :+: :+: :+:   +:+:+: :+:+:+ :+:    :+: :+:    :+              
#  +:+ +:+ +:+   +:+  +:+ +:+:+ +:+ +:+    +:+ +:+    +:+             
#   +#++: +#++:++#++: +#+  +:+  +#+ +#++:++#+  +#+    +:+             
#    +#+  +#+     +#+ +#+       +#+ +#+    +#+ +#+    +#+             
#    #+#  #+#     #+# #+#       #+# #+#    #+# #+#    #+#             
#    ###  ###     ### ###       ### #########   ########              
#                                                                     
#                                                                     
#       Version 5.0.1 Revision 19545 Hash fc76a9c4f                   
#                        Branch is                                    
#                    MPI+HDF5_IO Build                                
#                http://www.yambo-code.org                            
#
optics                           # [R] Linear Response optical properties
em1s                             # [R][Xs] Statically Screened Interaction
bss                              # [R] BSE solver
bse                              # [R][BSE] Bethe Salpeter Equation.
dipoles                          # [R] Oscillator strenghts (or dipoles)
StdoHash=  40                    # [IO] Live-timing Hashes
Nelectro= 52.00000               # Electrons number
ElecTemp= 0.000000         eV    # Electronic Temperature
BoseTemp=-1.000000         eV    # Bosonic Temperature
OccTresh= 0.100000E-4            # Occupation treshold (metallic bands)
NLogCPUs=0                       # [PARALLEL] Live-timing CPU`s (0 for all)
DBsIOoff= "none"                 # [IO] Space-separated list of DB with NO I/O. DB=(DIP,X,HF,COLLs,J,GF,CARRIERs,OBS,W,SC,BS,ALL)
DBsFRAGpm= "none"                # [IO] Space-separated list of +DB to FRAG and -DB to NOT FRAG. DB=(DIP,X,W,HF,COLLS,K,BS,QINDX,RT,ELP
FFTGvecs=  20           Ha    # [FFT] Plane-waves
#WFbuffIO                      # [IO] Wave-functions buffered I/O
PAR_def_mode= "balanced"         # [PARALLEL] Default distribution mode ("balanced"/"memory"/"workload")
DIP_CPU= "1.48.15"                      # [PARALLEL] CPUs for each role
DIP_ROLEs= "k.c.v"                    # [PARALLEL] CPUs roles (k,c,v)
X_and_IO_CPU= "1.1.1.48.15"                 # [PARALLEL] CPUs for each role
X_and_IO_ROLEs= "q.g.k.c.v"               # [PARALLEL] CPUs roles (q,g,k,c,v)
X_and_IO_nCPU_LinAlg_INV=-1      # [PARALLEL] CPUs for Linear Algebra (if -1 it is automatically set)
BS_CPU= "7.1.7"                       # [PARALLEL] CPUs for each role
BS_ROLEs= "k.eh.t"                     # [PARALLEL] CPUs roles (k,eh,t)
BS_nCPU_LinAlg_INV=-1            # [PARALLEL] CPUs for Linear Algebra (if -1 it is automatically set)
BS_nCPU_LinAlg_DIAGO=-1          # [PARALLEL] CPUs for Linear Algebra (if -1 it is automatically set)
NonPDirs= "none"                 # [X/BSS] Non periodic chartesian directions (X,Y,Z,XY...)
% MolPos
 0.000000 | 0.000000 | 0.000000 |        # [X/BSS] Molecule coord in supercell, 0.5 is the middle
%
Chimod= "HARTREE"                # [X] IP/Hartree/ALDA/LRC/PF/BSfxc
ChiLinAlgMod= "LIN_SYS"          # [X] inversion/lin_sys,cpu/gpu
BSEmod= "resonant"               # [BSE] resonant/retarded/coupling
BSKmod= "SEX"                    # [BSE] IP/Hartree/HF/ALDA/SEX/BSfxc
Lkind= "Lbar"                    # [BSE] Lbar (default) / full
BSSmod= "d"                      # [BSS] (h)aydock/(d)iagonalization/(s)lepc/(i)nversion/(t)ddft`
% DipBands
    1 | 1250 |                       # [DIP] Bands range for dipoles
%
#DipBandsALL                   # [DIP] Compute all bands range, not only valence and conduction
DipApproach= "G-space v"         # [DIP] [G-space v/R-space x/Covariant/Shifted grids]
DipComputed= "R P V"             # [DIP] [default R P V; extra P2 Spin Orb]
#DipPDirect                    # [DIP] Directly compute <v> also when using other approaches for dipoles
ShiftedPaths= ""                 # [DIP] Shifted grids paths (separated by a space)
DbGdQsize= 1.000000              # [X,DbGd][o/o] Percentual of the total DbGd transitions to be used
Gauge= "length"                  # [BSE/X] Gauge (length|velocity)
#AnHall                        # [BSE] Add the anomalous Hall effect to eps if using length gauge
BSENGexx= 10           Ha    # [BSK] Exchange components
#ALLGexx                       # [BSS] Force the use use all RL vectors for the exchange part
BSENGBlk= 4            Ha    # [BSK] Screened interaction block size [if -1 uses all the G-vectors of W(q,G,Gp)]
#WehDiag                       # [BSK] diagonal (G-space) the eh interaction
#WehCpl                        # [BSK] eh interaction included also in coupling
KfnQPdb= "E < gw-dyn-no-rim/ndb.QP"                  # [EXTQP BSK BSS] Database action
KfnQP_INTERP_NN= 10               # [EXTQP BSK BSS] Interpolation neighbours (NN mode)
KfnQP_INTERP_shells= 40.00000    # [EXTQP BSK BSS] Interpolation shells (BOLTZ mode)
KfnQP_DbGd_INTERP_mode= "BOLTZ"     # [EXTQP BSK BSS] Interpolation DbGd mode
% KfnQP_E
 0.0000000 | 1.000000 | 1.000000 |        # [EXTQP BSK BSS] E parameters  (c/v) eV|adim|adim
%
KfnQP_Z= ( 1.000000 , 0.000000 )         # [EXTQP BSK BSS] Z factor  (c/v)
KfnQP_Wv_E= 0.000000       eV    # [EXTQP BSK BSS] W Energy reference  (valence)
% KfnQP_Wv
 0.000000 | 0.000000 | 0.000000 |        # [EXTQP BSK BSS] W parameters  (valence) eV| 1|eV^-1
%
KfnQP_Wv_dos= 0.000000     eV    # [EXTQP BSK BSS] W dos pre-factor  (valence)
KfnQP_Wc_E= 0.000000       eV    # [EXTQP BSK BSS] W Energy reference  (conduction)
% KfnQP_Wc
 0.000000 | 0.000000 | 0.000000 |        # [EXTQP BSK BSS] W parameters  (conduction) eV| 1 |eV^-1
%
KfnQP_Wc_dos= 0.000000     eV    # [EXTQP BSK BSS] W dos pre-factor  (conduction)
#NoCondSumRule                 # [BSE/X] Do not impose the conductivity sum rule in velocity gauge
#MetDamp                       # [BSE] Define \w+=sqrt(\w*(\w+i\eta))
BSEprop= "abs"                   # [BSS] abs/kerr/magn/dichr trace
% PL_weights
 1.000000 | 1.000000 | 1.000000 |        # [PL] [cc] Weights of the carthesian components of the emitted radiation
%
DrudeWBS= ( 0.000000 , 0.000000 )  eV    # [BSE] Drude plasmon
#Reflectivity                  # [BSS] Compute reflectivity at normal incidence
BoseCut= 0.100000                # [BOSE] Finite T Bose function cutoff
% BSEQptR
 1 | 1 |                             # [BSK] Transferred momenta range
%
% BSEBands
    49 | 56 |                       # [BSK] Bands range
%
BSKCut= 0.000000                 # [BSK] Cutoff on the BSE Kernel, 0=full 1=none
% BSEEhEny
-1.000000 |-1.000000 |         eV    # [BSK] Electron-hole energy range
%
% BSehWind
 10.0000 | 10.0000 |               # [BSK] [o/o] E/h coupling pairs energy window
%
% BEnRange
  0.00000 | 4.00000 |         eV    # [BSS] Energy range
%
% BDmRange
 0.100000 | 0.100000 |         eV    # [BSS] Damping range
%
BDmERef= 0.000000          eV    # [BSS] Damping energy reference
BEnSteps= 100                    # [BSS] Energy steps
% BLongDir
 1.000000 | 1.000000 | 0.000000 |        # [BSS] [cc] Electric Field
%
WRbsWF                        # [BSS] Write to disk excitonic the WFs
#BSSPertWidth                  # [BSS] Include QPs lifetime in a perturbative way
XfnQPdb= "none"                  # [EXTQP Xd] Database action
XfnQP_INTERP_NN= 1               # [EXTQP Xd] Interpolation neighbours (NN mode)
XfnQP_INTERP_shells= 20.00000    # [EXTQP Xd] Interpolation shells (BOLTZ mode)
XfnQP_DbGd_INTERP_mode= "NN"     # [EXTQP Xd] Interpolation DbGd mode
% XfnQP_E
 0.000000 | 1.000000 | 1.000000 |        # [EXTQP Xd] E parameters  (c/v) eV|adim|adim
%
XfnQP_Z= ( 1.000000 , 0.000000 )         # [EXTQP Xd] Z factor  (c/v)
XfnQP_Wv_E= 0.000000       eV    # [EXTQP Xd] W Energy reference  (valence)
% XfnQP_Wv
 0.000000 | 0.000000 | 0.000000 |        # [EXTQP Xd] W parameters  (valence) eV| 1|eV^-1
%
XfnQP_Wv_dos= 0.000000     eV    # [EXTQP Xd] W dos pre-factor  (valence)
XfnQP_Wc_E= 0.000000       eV    # [EXTQP Xd] W Energy reference  (conduction)
% XfnQP_Wc
 0.000000 | 0.000000 | 0.000000 |        # [EXTQP Xd] W parameters  (conduction) eV| 1 |eV^-1
%
XfnQP_Wc_dos= 0.000000     eV    # [EXTQP Xd] W dos pre-factor  (conduction)
% QpntsRXs
  1 |  7 |                           # [Xs] Transferred momenta
%
% BndsRnXs
    1 | 1250 |                       # [Xs] Polarization function bands
%
NGsBlkXs= 4                Ha    # [Xs] Response block size
GrFnTpXs= "T"                    # [Xs] Green`s function (T)ordered,(R)etarded,(r)senant,(a)ntiresonant [T, R, r, Ta, Ra]
% DmRngeXs
 0.100000E-2 | 0.100000E-2 |   eV    # [Xs] Damping range
%
CGrdSpXs= 100.0000               # [Xs] [o/o] Coarse grid controller
% EhEngyXs
-1.000000 |-1.000000 |         eV    # [Xs] Electron-hole energy range
%
% LongDrXs
 1.000000 | 0.000000 | 0.000000 |        # [Xs] [cc] Electric Field
%
DrudeWXs= ( 0.000000 , 0.000000 )  eV    # [Xs] Drude plasmon
XTermKind= "none"                # [X] X terminator ("none","BG" Bruneval-Gonze)
XTermEn= 40.00000          eV    # [X] X terminator energy (only for kind="BG")
