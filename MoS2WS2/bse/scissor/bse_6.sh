#!/bin/bash
#SBATCH --nodes=16              # Number of Nodes
#SBATCH --qos=debug
#SBATCH --ntasks  320
##SBATCH --ntasks-per-node=  # Number of MPI tasks per node
#SBATCH --cpus-per-task=1       # Number of OpenMP threads
##SBATCH --hint=nomultithread    # Disable hyperthreading
#SBATCH --job-name=diago6b     # Job name
#SBATCH --output=gwall6b.out   # Output file %x is the jobname, %j the jobid
#SBATCH --error=gwall6b.err          # Error file
#SBATCH --time=02:00:00         #Expected runtime HH:MM:SS (max 100h)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=r.reho@uu.nl
#SBATCH --dependency=afterok:17447959
#SBATCH --constraint=highmem
##SBATCH --begin=now+1hour
#module purge
source ~/modules_yambo.load
  
mpirun -np 320  yambo -F bse_6.in -J 'bse,bse_6' -C bse_6 -I ..//..//
