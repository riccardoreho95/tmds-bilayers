#!/bin/bash
#SBATCH --nodes=1              # Number of Nodes
#SBATCH --qos=debug
#SBATCH --ntasks-per-node=48   # Number of MPI tasks per node
#SBATCH --cpus-per-task=1       # Number of OpenMP threads
##SBATCH --hint=nomultithread    # Disable hyperthreading
#SBATCH --job-name=exciton_plot     # Job name
#SBATCH --output=gwall.out   # Output file %x is the jobname, %j the jobid
#SBATCH --error=gwall.err          # Error file
#SBATCH --time=02:00:00         #Expected runtime HH:MM:SS (max 100h)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=r.reho@uu.nl
##SBATCH --dependency=afterok:16468321
##SBATCH --constraint=highmem
#module purge
source ~/modules_yambo.load

ypp -F ypp_WF_3.in -J 'bse,second_exciton' -I ..//..//
